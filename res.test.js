const funcs = require("./sum");

test("subs 5 - 2 to equal 3", () => {
  expect(funcs.sub(5, 2)).toBe(3);
});
