const funcs = require("./sum");

test("adds 1 + 2 to equal 3", () => {
  expect(funcs.sum(1, 2)).toBe(3);
});

test("multiply 10 * 10 equal 100", () => {
  expect(funcs.mult(10, 10)).toBe(100);
});
